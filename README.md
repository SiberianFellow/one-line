# Download 196 (001 – 196) issues of the magazine [«Хакер»](https://xakep.ru/) in one line of code!
![picture](https://xakep.ru/wp-content/uploads/2016/09/xakep_logo_big.png)
* * *
## Setup

💾 Available space: 8 Gb

🐍 [Python3](https://www.python.org/downloads/) + library *requests*
* * *
## Usage 
⌨️
~~~
python -c "import requests; [open(str(i) + '.pdf', 'wb').write((requests.get('https://xakep.ru/pdf/xa/' + '00' + str(i) if i < 10 else ('0' + str(i) if (i > 9 and i < 100) else str(i)), allow_redirects=True)).content) for i in range(1, 197)]"
~~~
* * *